{
  "name": "sklearn.ensemble.bagging.BaggingClassifier",
  "id": "e06805c141571e0c719f1bfb8ca987d0",
  "common_name": "BaggingClassifier",
  "is_class": true,
  "tags": [
    "ensemble",
    "bagging"
  ],
  "version": "0.19.1",
  "source_code": "https://github.com/scikit-learn/scikit-learn/blob/0.19.1/sklearn/ensemble/bagging.py#L429",
  "parameters": [
    {
      "type": "object",
      "optional": "true",
      "default": "None",
      "name": "base_estimator",
      "description": "The base estimator to fit on random subsets of the dataset. If None, then the base estimator is a decision tree. "
    },
    {
      "type": "int",
      "optional": "true",
      "default": "10",
      "name": "n_estimators",
      "description": "The number of base estimators in the ensemble. "
    },
    {
      "type": "int",
      "optional": "true",
      "default": "1.0",
      "name": "max_samples",
      "description": "The number of samples to draw from X to train each base estimator. - If int, then draw `max_samples` samples. - If float, then draw `max_samples * X.shape[0]` samples. "
    },
    {
      "type": "int",
      "optional": "true",
      "default": "1.0",
      "name": "max_features",
      "description": "The number of features to draw from X to train each base estimator. - If int, then draw `max_features` features. - If float, then draw `max_features * X.shape[1]` features. "
    },
    {
      "type": "boolean",
      "optional": "true",
      "default": "True",
      "name": "bootstrap",
      "description": "Whether samples are drawn with replacement. "
    },
    {
      "type": "boolean",
      "optional": "true",
      "default": "False",
      "name": "bootstrap_features",
      "description": "Whether features are drawn with replacement. "
    },
    {
      "type": "bool",
      "name": "oob_score",
      "description": "Whether to use out-of-bag samples to estimate the generalization error. "
    },
    {
      "type": "bool",
      "optional": "true",
      "default": "False",
      "name": "warm_start",
      "description": "When set to True, reuse the solution of the previous call to fit and add more estimators to the ensemble, otherwise, just fit a whole new ensemble.  .. versionadded:: 0.17 *warm_start* constructor parameter. "
    },
    {
      "type": "int",
      "optional": "true",
      "default": "1",
      "name": "n_jobs",
      "description": "The number of jobs to run in parallel for both `fit` and `predict`. If -1, then the number of jobs is set to the number of cores. "
    },
    {
      "type": "int",
      "optional": "true",
      "default": "None",
      "name": "random_state",
      "description": "If int, random_state is the seed used by the random number generator; If RandomState instance, random_state is the random number generator; If None, the random number generator is the RandomState instance used by `np.random`. "
    },
    {
      "type": "int",
      "optional": "true",
      "default": "0",
      "name": "verbose",
      "description": "Controls the verbosity of the building process. "
    }
  ],
  "attributes": [
    {
      "type": "estimator",
      "name": "base_estimator_",
      "description": "The base estimator from which the ensemble is grown. "
    },
    {
      "type": "list",
      "name": "estimators_",
      "description": "The collection of fitted base estimators. "
    },
    {
      "type": "list",
      "name": "estimators_samples_",
      "description": "The subset of drawn samples (i.e., the in-bag samples) for each base estimator. Each subset is defined by a boolean mask. "
    },
    {
      "type": "list",
      "name": "estimators_features_",
      "description": "The subset of drawn features for each base estimator. "
    },
    {
      "type": "array",
      "shape": "n_classes",
      "name": "classes_",
      "description": "The classes labels. "
    },
    {
      "type": "int",
      "name": "n_classes_",
      "description": "The number of classes. "
    },
    {
      "type": "float",
      "name": "oob_score_",
      "description": "Score of the training dataset obtained using an out-of-bag estimate. "
    },
    {
      "type": "array",
      "shape": "n_samples, n_classes",
      "name": "oob_decision_function_",
      "description": "Decision function computed with out-of-bag estimate on the training set. If n_estimators is small it might be possible that a data point was never left out during the bootstrap. In this case, `oob_decision_function_` might contain NaN. "
    }
  ],
  "description": "'A Bagging classifier.\n\nA Bagging classifier is an ensemble meta-estimator that fits base\nclassifiers each on random subsets of the original dataset and then\naggregate their individual predictions (either by voting or by averaging)\nto form a final prediction. Such a meta-estimator can typically be used as\na way to reduce the variance of a black-box estimator (e.g., a decision\ntree), by introducing randomization into its construction procedure and\nthen making an ensemble out of it.\n\nThis algorithm encompasses several works from the literature. When random\nsubsets of the dataset are drawn as random subsets of the samples, then\nthis algorithm is known as Pasting [1]_. If samples are drawn with\nreplacement, then the method is known as Bagging [2]_. When random subsets\nof the dataset are drawn as random subsets of the features, then the method\nis known as Random Subspaces [3]_. Finally, when base estimators are built\non subsets of both samples and features, then the method is known as\nRandom Patches [4]_.\n\nRead more in the :ref:`User Guide <bagging>`.\n",
  "methods_available": [
    {
      "name": "decision_function",
      "id": "sklearn.ensemble.bagging.BaggingClassifier.decision_function",
      "parameters": [
        {
          "type": "array-like, sparse matrix",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "The training input samples. Sparse matrices are accepted only if they are supported by the base estimator. "
        }
      ],
      "description": "'Average of the decision functions of the base classifiers.\n",
      "returns": {
        "type": "array",
        "shape": "n_samples, k",
        "name": "score",
        "description": "The decision function of the input samples. The columns correspond to the classes in sorted order, as they appear in the attribute ``classes_``. Regression and binary classification are special cases with ``k == 1``, otherwise ``k==n_classes``.  '"
      }
    },
    {
      "name": "fit",
      "id": "sklearn.ensemble.bagging.BaggingClassifier.fit",
      "parameters": [
        {
          "type": "array-like, sparse matrix",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "The training input samples. Sparse matrices are accepted only if they are supported by the base estimator. "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "name": "y",
          "description": "The target values (class labels in classification, real numbers in regression). "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "name": "sample_weight",
          "description": "Sample weights. If None, then samples are equally weighted. Note that this is supported only if the base estimator supports sample weighting. "
        }
      ],
      "description": "'Build a Bagging ensemble of estimators from the training\nset (X, y).\n",
      "returns": {
        "type": "object",
        "name": "self",
        "description": "Returns self. '"
      }
    },
    {
      "name": "get_params",
      "id": "sklearn.ensemble.bagging.BaggingClassifier.get_params",
      "parameters": [
        {
          "type": "boolean",
          "optional": "true",
          "name": "deep",
          "description": "If True, will return the parameters for this estimator and contained subobjects that are estimators. "
        }
      ],
      "description": "'Get parameters for this estimator.\n",
      "returns": {
        "type": "mapping",
        "name": "params",
        "description": "Parameter names mapped to their values. '"
      }
    },
    {
      "name": "predict",
      "id": "sklearn.ensemble.bagging.BaggingClassifier.predict",
      "parameters": [
        {
          "type": "array-like, sparse matrix",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "The training input samples. Sparse matrices are accepted only if they are supported by the base estimator. "
        }
      ],
      "description": "'Predict class for X.\n\nThe predicted class of an input sample is computed as the class with\nthe highest mean predicted probability. If base estimators do not\nimplement a ``predict_proba`` method, then it resorts to voting.\n",
      "returns": {
        "type": "array",
        "shape": "n_samples",
        "name": "y",
        "description": "The predicted classes. '"
      }
    },
    {
      "name": "predict_log_proba",
      "id": "sklearn.ensemble.bagging.BaggingClassifier.predict_log_proba",
      "parameters": [
        {
          "type": "array-like, sparse matrix",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "The training input samples. Sparse matrices are accepted only if they are supported by the base estimator. "
        }
      ],
      "description": "'Predict class log-probabilities for X.\n\nThe predicted class log-probabilities of an input sample is computed as\nthe log of the mean predicted class probabilities of the base\nestimators in the ensemble.\n",
      "returns": {
        "type": "array",
        "shape": "n_samples, n_classes",
        "name": "p",
        "description": "The class log-probabilities of the input samples. The order of the classes corresponds to that in the attribute `classes_`. '"
      }
    },
    {
      "name": "predict_proba",
      "id": "sklearn.ensemble.bagging.BaggingClassifier.predict_proba",
      "parameters": [
        {
          "type": "array-like, sparse matrix",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "The training input samples. Sparse matrices are accepted only if they are supported by the base estimator. "
        }
      ],
      "description": "'Predict class probabilities for X.\n\nThe predicted class probabilities of an input sample is computed as\nthe mean predicted class probabilities of the base estimators in the\nensemble. If base estimators do not implement a ``predict_proba``\nmethod, then it resorts to voting and the predicted class probabilities\nof an input sample represents the proportion of estimators predicting\neach class.\n",
      "returns": {
        "type": "array",
        "shape": "n_samples, n_classes",
        "name": "p",
        "description": "The class probabilities of the input samples. The order of the classes corresponds to that in the attribute `classes_`. '"
      }
    },
    {
      "name": "score",
      "id": "sklearn.ensemble.bagging.BaggingClassifier.score",
      "parameters": [
        {
          "type": "array-like",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "Test samples. "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "name": "y",
          "description": "True labels for X. "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "optional": "true",
          "name": "sample_weight",
          "description": "Sample weights. "
        }
      ],
      "description": "'Returns the mean accuracy on the given test data and labels.\n\nIn multi-label classification, this is the subset accuracy\nwhich is a harsh metric since you require for each sample that\neach label set be correctly predicted.\n",
      "returns": {
        "type": "float",
        "name": "score",
        "description": "Mean accuracy of self.predict(X) wrt. y.  '"
      }
    },
    {
      "name": "set_params",
      "id": "sklearn.ensemble.bagging.BaggingClassifier.set_params",
      "parameters": [],
      "description": "\"Set the parameters of this estimator.\n\nThe method works on simple estimators as well as on nested objects\n(such as pipelines). The latter have parameters of the form\n``<component>__<parameter>`` so that it's possible to update each\ncomponent of a nested object.\n",
      "returns": {
        "name": "self",
        "description": "\""
      }
    }
  ]
}