{
  "name": "sklearn.linear_model.perceptron.Perceptron",
  "id": "bde46a3ea8e20fcd1001b47201fef47a",
  "common_name": "Perceptron",
  "is_class": true,
  "tags": [
    "linear_model",
    "perceptron"
  ],
  "version": "0.19.1",
  "source_code": "https://github.com/scikit-learn/scikit-learn/blob/0.19.1/sklearn/linear_model/perceptron.py#L7",
  "parameters": [
    {
      "type": "",
      "name": "penalty",
      "description": "The penalty (aka regularization term) to be used. Defaults to None. "
    },
    {
      "type": "float",
      "name": "alpha",
      "description": "Constant that multiplies the regularization term if regularization is used. Defaults to 0.0001 "
    },
    {
      "type": "bool",
      "name": "fit_intercept",
      "description": "Whether the intercept should be estimated or not. If False, the data is assumed to be already centered. Defaults to True. "
    },
    {
      "type": "int",
      "optional": "true",
      "name": "max_iter",
      "description": "The maximum number of passes over the training data (aka epochs). It only impacts the behavior in the ``fit`` method, and not the `partial_fit`. Defaults to 5. Defaults to 1000 from 0.21, or if tol is not None.  .. versionadded:: 0.19 "
    },
    {
      "type": "float",
      "optional": "true",
      "name": "tol",
      "description": "The stopping criterion. If it is not None, the iterations will stop when (loss > previous_loss - tol). Defaults to None. Defaults to 1e-3 from 0.21.  .. versionadded:: 0.19 "
    },
    {
      "type": "bool",
      "optional": "true",
      "name": "shuffle",
      "description": "Whether or not the training data should be shuffled after each epoch. "
    },
    {
      "type": "integer",
      "optional": "true",
      "name": "verbose",
      "description": "The verbosity level "
    },
    {
      "type": "double",
      "name": "eta0",
      "description": "Constant by which the updates are multiplied. Defaults to 1. "
    },
    {
      "type": "integer",
      "optional": "true",
      "name": "n_jobs",
      "description": "The number of CPUs to use to do the OVA (One Versus All, for multi-class problems) computation. -1 means \\'all CPUs\\'. Defaults to 1. "
    },
    {
      "type": "int",
      "optional": "true",
      "name": "random_state",
      "description": "The seed of the pseudo random number generator to use when shuffling the data.  If int, random_state is the seed used by the random number generator; If RandomState instance, random_state is the random number generator; If None, the random number generator is the RandomState instance used by `np.random`. "
    },
    {
      "type": "dict",
      "name": "class_weight",
      "description": "Preset for the class_weight fit parameter.  Weights associated with classes. If not given, all classes are supposed to have weight one.  The \"balanced\" mode uses the values of y to automatically adjust weights inversely proportional to class frequencies in the input data as ``n_samples / (n_classes * np.bincount(y))`` "
    },
    {
      "type": "bool",
      "optional": "true",
      "name": "warm_start",
      "description": "When set to True, reuse the solution of the previous call to fit as initialization, otherwise, just erase the previous solution. "
    },
    {
      "type": "int",
      "optional": "true",
      "name": "n_iter",
      "description": "The number of passes over the training data (aka epochs). Defaults to None. Deprecated, will be removed in 0.21.  .. versionchanged:: 0.19 Deprecated "
    }
  ],
  "attributes": [
    {
      "type": "array",
      "shape": "1, n_features",
      "name": "coef_",
      "description": "Weights assigned to the features. "
    },
    {
      "type": "array",
      "shape": "1",
      "name": "intercept_",
      "description": "Constants in decision function. "
    },
    {
      "type": "int",
      "name": "n_iter_",
      "description": "The actual number of iterations to reach the stopping criterion. For multiclass fits, it is the maximum over every binary fit. "
    }
  ],
  "description": "'Perceptron\n\nRead more in the :ref:`User Guide <perceptron>`.\n",
  "methods_available": [
    {
      "name": "decision_function",
      "id": "sklearn.linear_model.perceptron.Perceptron.decision_function",
      "parameters": [
        {
          "type": "array-like, sparse matrix",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "Samples. "
        }
      ],
      "description": "'Predict confidence scores for samples.\n\nThe confidence score for a sample is the signed distance of that\nsample to the hyperplane.\n",
      "returns": {
        "name": "array, shape=(n_samples,) if n_classes == 2 else (n_samples, n_classes)",
        "description": "Confidence scores per (sample, class) combination. In the binary case, confidence score for self.classes_[1] where >0 means this class would be predicted. '"
      }
    },
    {
      "name": "densify",
      "id": "sklearn.linear_model.perceptron.Perceptron.densify",
      "parameters": [],
      "description": "'Convert coefficient matrix to dense array format.\n\nConverts the ``coef_`` member (back) to a numpy.ndarray. This is the\ndefault format of ``coef_`` and is required for fitting, so calling\nthis method is only required on models that have previously been\nsparsified; otherwise, it is a no-op.\n",
      "returns": {
        "type": "estimator",
        "name": "self",
        "description": "'"
      }
    },
    {
      "name": "fit",
      "id": "sklearn.linear_model.perceptron.Perceptron.fit",
      "parameters": [
        {
          "type": "array-like, sparse matrix",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "Training data "
        },
        {
          "type": "numpy",
          "shape": "n_samples,",
          "name": "y",
          "description": "Target values "
        },
        {
          "type": "array",
          "shape": "n_classes, n_features",
          "name": "coef_init",
          "description": "The initial coefficients to warm-start the optimization. "
        },
        {
          "type": "array",
          "shape": "n_classes,",
          "name": "intercept_init",
          "description": "The initial intercept to warm-start the optimization. "
        },
        {
          "type": "array-like",
          "shape": "n_samples,",
          "optional": "true",
          "name": "sample_weight",
          "description": "Weights applied to individual samples. If not provided, uniform weights are assumed. These weights will be multiplied with class_weight (passed through the constructor) if class_weight is specified "
        }
      ],
      "description": "'Fit linear model with Stochastic Gradient Descent.\n",
      "returns": {
        "type": "returns",
        "name": "self",
        "description": "'"
      }
    },
    {
      "name": "get_params",
      "id": "sklearn.linear_model.perceptron.Perceptron.get_params",
      "parameters": [
        {
          "type": "boolean",
          "optional": "true",
          "name": "deep",
          "description": "If True, will return the parameters for this estimator and contained subobjects that are estimators. "
        }
      ],
      "description": "'Get parameters for this estimator.\n",
      "returns": {
        "type": "mapping",
        "name": "params",
        "description": "Parameter names mapped to their values. '"
      }
    },
    {
      "name": "partial_fit",
      "id": "sklearn.linear_model.perceptron.Perceptron.partial_fit",
      "parameters": [
        {
          "type": "array-like, sparse matrix",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "Subset of the training data "
        },
        {
          "type": "numpy",
          "shape": "n_samples,",
          "name": "y",
          "description": "Subset of the target values "
        },
        {
          "type": "array",
          "shape": "n_classes,",
          "name": "classes",
          "description": "Classes across all calls to partial_fit. Can be obtained by via `np.unique(y_all)`, where y_all is the target vector of the entire dataset. This argument is required for the first call to partial_fit and can be omitted in the subsequent calls. Note that y doesn't need to contain all labels in `classes`. "
        },
        {
          "type": "array-like",
          "shape": "n_samples,",
          "optional": "true",
          "name": "sample_weight",
          "description": "Weights applied to individual samples. If not provided, uniform weights are assumed. "
        }
      ],
      "description": "\"Fit linear model with Stochastic Gradient Descent.\n",
      "returns": {
        "type": "returns",
        "name": "self",
        "description": "\""
      }
    },
    {
      "name": "predict",
      "id": "sklearn.linear_model.perceptron.Perceptron.predict",
      "parameters": [
        {
          "type": "array-like, sparse matrix",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "Samples. "
        }
      ],
      "description": "'Predict class labels for samples in X.\n",
      "returns": {
        "type": "array",
        "shape": "n_samples",
        "name": "C",
        "description": "Predicted class label per sample. '"
      }
    },
    {
      "name": "score",
      "id": "sklearn.linear_model.perceptron.Perceptron.score",
      "parameters": [
        {
          "type": "array-like",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "Test samples. "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "name": "y",
          "description": "True labels for X. "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "optional": "true",
          "name": "sample_weight",
          "description": "Sample weights. "
        }
      ],
      "description": "'Returns the mean accuracy on the given test data and labels.\n\nIn multi-label classification, this is the subset accuracy\nwhich is a harsh metric since you require for each sample that\neach label set be correctly predicted.\n",
      "returns": {
        "type": "float",
        "name": "score",
        "description": "Mean accuracy of self.predict(X) wrt. y.  '"
      }
    },
    {
      "name": "set_params",
      "id": "sklearn.linear_model.perceptron.Perceptron.set_params",
      "parameters": [],
      "description": "None"
    },
    {
      "name": "sparsify",
      "id": "sklearn.linear_model.perceptron.Perceptron.sparsify",
      "parameters": [],
      "description": "'Convert coefficient matrix to sparse format.\n\nConverts the ``coef_`` member to a scipy.sparse matrix, which for\nL1-regularized models can be much more memory- and storage-efficient\nthan the usual numpy.ndarray representation.\n\nThe ``intercept_`` member is not converted.\n\nNotes\n-----\nFor non-sparse models, i.e. when there are not many zeros in ``coef_``,\nthis may actually *increase* memory usage, so use this method with\ncare. A rule of thumb is that the number of zero elements, which can\nbe computed with ``(coef_ == 0).sum()``, must be more than 50% for this\nto provide significant benefits.\n\nAfter calling this method, further fitting with the partial_fit\nmethod (if any) will not work until you call densify.\n",
      "returns": {
        "type": "estimator",
        "name": "self",
        "description": "'"
      }
    }
  ]
}